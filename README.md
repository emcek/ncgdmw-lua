# NCGDMW Lua Edition

A "Natural Grow" leveling mod, where your attributes grow automatically as your skills increase and your skills will also decay over time (Optional).

A changelog is available in the [CHANGELOG.md](CHANGELOG.md) file, and an FAQ in the [FAQ.md](FAQ.md) file.

#### Credits

This is a port of [Greywander's "Natural Character Growth and Decay - MW" mod](https://www.nexusmods.com/morrowind/mods/44967) for the OpenMW-Lua API.

##### Original concept, MWScript edition author

Greywander

##### Lua edition authors

EvilEye, johnnyhostile

##### Localization

BR: Karolz

DE: Atahualpa

ES: drumvee

FR: [Rob from Rob's Red Hot Spot](https://www.youtube.com/channel/UCue7D0dm2SilBhbVe3SB75g)

RU: [dmbaturin](https://baturin.org/)

#### Installation

**OpenMW 0.48 or newer is required!**

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/ncgdmw-lua/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\Leveling\NCGDMW-Lua

        # Linux
        /home/username/games/OpenMWMods/Leveling/NCGDMW-Lua

        # macOS
        /Users/username/games/OpenMWMods/Leveling/NCGDMW-Lua

1. Add the appropriate data path to your `opemw.cfg` file (e.g. `data="C:\games\OpenMWMods\Leveling\NCGDMW-Lua"`)
1. Add `content=ncgdmw.omwaddon` and `content=ncgdmw.omwscripts` to your load order in `openmw.cfg` or enable them via OpenMW-Launcher
1. Enjoy!

**When you finish selecting your character class and sign and get your papers, NCGDMW will begin. Press ESC and navigate to: Options >> Scripts >> NCGDMW in order to set up the mod's options to your liking.**

#### Configuring

You will be prompted once the mod has activated, after you've selected your class and birthsign. You can change your mod settings at any time via the in-game mod settings menu.

##### Using A Localization

To use a specific localization supported by this mod, your `settings.cfg` file must contain the following:

```
[General]
preferred locales = br,en
<other options may be below here>
```

The above example prefers BR locales but will use EN when anything isn't available. See [the official OpenMW documentation on this option](https://openmw.readthedocs.io/en/latest/reference/modding/settings/general.html?highlight=preferred%20locales#preferred-locales) for more information.

#### How Decay Works

The decay mechanic can be a nice way to balance the game and stave off becoming too powerful too quickly. Some things to keep in mind about this mechanic:

* Decay won't begin right away; depending on the speed you've selected it could take between three and seventeen days of game time to begin.
* A skill cannot decay past one half of it's previous maximum value. So if your Acrobatics skill was at 50, it can't decay past 25.
* Regardless of a skill's max value, it will not decay below 15.
* When you level up a skill, a small amount of decay progress is removed.

#### Help Localize NCGDMW

Do you speak a language that's not yet offered and want to contribute a new localization? Follow these steps:

1. Download a release zip from [this URL](https://modding-openmw.gitlab.io/ncgdmw-lua/)
1. Open the `l10n/NCGDMW/en.yaml` file with your favorite text editor ([Notepad++](https://notepad-plus-plus.org/) is recommended for Windows)
1. Update each line (the quoted part after the `:`) as desired
1. Save the file with the name `<CODE>.yaml`, where `<CODE>` is the [language code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) for the localization you're adding
1. Commit your change to git and [open a merge request](https://gitlab.com/modding-openmw/ncgdmw-lua/-/merge_requests/new), or simply email the file to `admin@modding-openmw.com` with `New NCGD Localization` as the subject

#### Planned Features

Not yet implemented (but possible):

* An inteface for other mods to hook into NCGDMW functionality

Not yet implemented (not yet possible):

* Post-chargen new game config popup menu
* Play a sound when a stat decays
* Uncapped attributes and skills
* Configurable caps for all/individual attributes/skills
* Optional level up song and animation
* View decay progress by mousing over a skill
* [Request a feature!](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/new)
